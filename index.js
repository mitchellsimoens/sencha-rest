'use strict';

module.exports = {
    get Endpoint () {
        return require('./Endpoint');
    },

    get Manager () {
        return require('./Manager');
    }
};
